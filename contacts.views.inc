<?php
/**
 * @file
 * Ctools content type.
 */

/**
 * Implements hook_views_data().
 */
function contacts_views_data() {

  $data['ext_contacts']['table']['base'] = array(
    'title' => t('My Contacts group'),
    'help' => t('Contacts records'),
  );

  $data['ext_contacts']['id'] = array(
    'title' => 'ID',
    'help'  => 'PostID',
    'group' => t('Contact'),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'sort'  => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );

  $data['ext_contacts']['name'] = array(
    'title'  => 'Name',
    'help'   => 'User name',
    'group'  => t('Contact'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'contacts_handler_filter_test'),
  );

  $data['ext_contacts']['surname'] = array(
    'title'  => 'Surname',
    'help'   => 'User surname',
    'group'  => t('Contact'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['nick'] = array(
    'title'  => 'Nickname',
    'help'   => 'User nickname',
    'group'  => t('Contact'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['email'] = array(
    'title'  => 'E-mail',
    'help'   => 'E-mail',
    'group'  => t('Contact'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['phone'] = array(
    'title'  => 'Phone number',
    'help'   => 'Phone number',
    'group'  => t('Contact'),
    'field' => array('handler' => 'views_handler_field'),
    'sort' => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['sex'] = array(
    'title'  => 'Sex',
    'help'   => 'Sex',
    'group'  => t('Contact'),
    'field'  => array('handler' => 'views_handler_field'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['country'] = array(
    'title'  => 'Country',
    'help'   => 'Country',
    'group'  => t('Contact'),
    'field'  => array('handler' => 'views_handler_field'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  $data['ext_contacts']['message'] = array(
    'title'  => 'Message',
    'help'   => 'User\'s message',
    'group'  => t('Contact'),
    'field'  => array('handler' => 'views_handler_field'),
    'sort'   => array('handler' => 'views_handler_sort'),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );

  return $data;
}
