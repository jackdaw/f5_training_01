<?php

$plugin = array(
  'title' => t('Ctools content type plugin'),
  'description' => t('Some text output'),
  'render callback' => 'contacts_cct_render',
  'edit form' => 'contacts_text_edit_form',
  'category' => t('Testing'),
  'single' => TRUE,
);

/**
 * Edit form for content type
 */
function contacts_text_edit_form($form, &$form_state) {

  $conf = $form_state['conf'];

  $form['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Please, enter custom text:'),
    '#default_value' => !empty($conf['text']) ? $conf['text'] : 'nothing here',
  );

  return $form;
}

/**
 * Submit contacts_text_edit_form()
 */
function contacts_text_edit_form_submit($form, &$form_state) {
  $form_state['conf'] = array(
    'text' => $form_state['values']['text'],
  );
}

/**
 * Rendering
 */
function contacts_cct_render($subtype, $conf, $args, $context) {
  /*if (empty($context) || empty($context->data) || (empty($conf['text'])) ) {
    return;
  }*/

  //$node = $context->data;

  $block = new stdClass();
  //$block->module = 'contacts';
  $block->content = $conf['text'];

  return $block;
}