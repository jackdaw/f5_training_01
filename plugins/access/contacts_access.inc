<?php
/**
 * @file
 * Ctools access plugin.
 */

$plugin = array(
  'title' => t("Contacts: access"),
  'description' => t('Access control'),
  'settings form' => 'contacts_access_settings_form',
  'summary' => 'contacts_pane_access_summary',
  'callback' => 'contacts_pane_access_check',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Access check.
 */
function contacts_pane_access_check($conf, $context) {
  return ($conf['cb'] == 0 || ( $conf['cb'] == 1 && $context->data->uid == 1 ) );
  /*else {
    drupal_access_denied();
    drupal_exit();
  }*/
}

/**
 * Settings form for access plugin.
 */
function contacts_access_settings_form(&$form, &$form_state, $conf) {

  $form['settings']['cb'] = array(
    '#type' => 'checkbox',
    '#title' => t('For admin only'),
    '#description' => t('Activate to display additional text for admin only.'),
    '#default_value' => $conf['cb'] == '1',
  );
  return $form;
}

/**
 * Short description output.
 */
function contacts_pane_access_summary($conf, $context) {
  return t('Display additional text for admin only.');
}