<?php
/**
 * Contacts string filter handler.
 */
class contacts_handler_filter_test extends views_handler_filter {
  function query() {
    $this->ensure_my_table();
    $values = array('s%' => 'abc%');
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->name like s%", array_values($values));
  }
}